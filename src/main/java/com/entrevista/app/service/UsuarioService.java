package com.entrevista.app.service;

import java.util.List;

import com.entrevista.app.dto.UsuarioDto;
import com.entrevista.app.request.UsuarioRequest;
import com.entrevista.app.utilerias.GenericResponse;


public interface UsuarioService {
	List<UsuarioDto> obtenerTodosLosUsuarios();
	GenericResponse altaUsuario(UsuarioRequest usuarioRequest);


}
