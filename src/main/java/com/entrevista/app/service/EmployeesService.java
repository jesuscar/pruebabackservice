package com.entrevista.app.service;

import com.entrevista.app.dto.EmployeHoursDto;
import com.entrevista.app.dto.EmployeeDto;
import com.entrevista.app.request.*;
import com.entrevista.app.utilerias.GenericResponsePrueba;

import java.util.ArrayList;

public interface EmployeesService {
	GenericResponsePrueba altaEmpleado(EmployeeRquest employeeRquest);
	EmployeeDto obtenerempleadoPorId( EmployeeRquest employeeRquest);
	EmployeHoursDto obtenerempleadoPorHoras( EmployedHorasRequest employedHorasRequest );
	GenericResponsePrueba altaEmpleadoHrs(EmployeedWorkedRequest request);
	ResponseJob obtenerEmpleadoJob(JobRequest request);
	GenericResponsePrueba obtenerTotalHrsFecha(totalHrsFechaRequest request);
	GenericResponsePrueba obtenerpagoEmpleadofecha(totalHrsFechaRequest request);

}
