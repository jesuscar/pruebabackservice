package com.entrevista.app.utilerias;

public class GenericResponsePrueba {
	private Integer id;
	private String success;
	public GenericResponsePrueba() {
		
	}
	public GenericResponsePrueba(Integer id, String success) {
		super();
		this.id = id;
		this.success = success;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	
	
}
