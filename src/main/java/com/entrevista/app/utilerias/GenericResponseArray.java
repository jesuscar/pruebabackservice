package com.entrevista.app.utilerias;

import com.entrevista.app.request.ResponseJob;

import java.util.ArrayList;

public class GenericResponseArray {
	private Integer codigo;
	 private ArrayList<ResponseJob> lista;
	public GenericResponseArray() {
		// TODO Auto-generated constructor stub
	}

	public GenericResponseArray(ArrayList<ResponseJob> lista) {
		this.lista = lista;
	}

	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public ArrayList<ResponseJob> getLista() {
		return lista;
	}

	public void setLista(ArrayList<ResponseJob> lista) {
		this.lista = lista;
	}
}
