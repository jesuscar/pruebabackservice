package com.entrevista.app.utilerias;

public class GenericResponse {
	private Integer codigo;
	private String mensaje;
	public GenericResponse() {
		// TODO Auto-generated constructor stub
	}
	public GenericResponse(Integer codigo, String mensaje) {
		super();
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
