package com.entrevista.app.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.entrevista.app.domain.Employees;

public class EmployeHoursDto {
	private Long idEmployWorked;
	private Employees employeeId;
	private Integer workedHours;
	private Date workedDate;
	private Integer Horas; 
	
	
	
	public EmployeHoursDto(Integer horas) {
		super();
		Horas = horas;
	}


	public Integer getHoras() {
		return Horas;
	}


	public void setHoras(Integer horas) {
		Horas = horas;
	}


	public EmployeHoursDto() {
		// TODO Auto-generated constructor stub
	}


	public EmployeHoursDto(Long idEmployWorked, Employees employeeId, Integer workedHours, Date workedDate) {
		super();
		this.idEmployWorked = idEmployWorked;
		this.employeeId = employeeId;
		this.workedHours = workedHours;
		this.workedDate = workedDate;
	}


	public Long getIdEmployWorked() {
		return idEmployWorked;
	}


	public void setIdEmployWorked(Long idEmployWorked) {
		this.idEmployWorked = idEmployWorked;
	}


	public Employees getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(Employees employeeId) {
		this.employeeId = employeeId;
	}


	public Integer getWorkedHours() {
		return workedHours;
	}


	public void setWorkedHours(Integer workedHours) {
		this.workedHours = workedHours;
	}


	public Date getWorkedDate() {
		return workedDate;
	}


	public void setWorkedDate(Date workedDate) {
		this.workedDate = workedDate;
	}

}
