package com.entrevista.app.dto;

public class UsuarioDto {
	private Long usuarioId;
	private String nombre;
	private String apellido;
	private Integer status;
	
	public UsuarioDto(Long usuarioId, String nombre, String apellido, Integer status) {
		super();
		this.usuarioId = usuarioId;
		this.nombre = nombre;
		this.apellido = apellido;
		this.status = status;
	}
	public UsuarioDto( String nombre, String apellido, Integer status) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.status = status;
	}

	public UsuarioDto() {
		
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
