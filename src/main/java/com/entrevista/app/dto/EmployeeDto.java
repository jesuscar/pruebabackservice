package com.entrevista.app.dto;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.entrevista.app.domain.Genders;
import com.entrevista.app.domain.Jobs;

public class EmployeeDto {
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

	private Long idEmploy;
	private String name;
	private String lastName;
	private Date birthday;
	private Genders gender;
	private Jobs job;

	public EmployeeDto(Long idEmploy, String name, String lastName, Date birthday, Genders gender, Jobs job) {
		super();
		this.idEmploy = idEmploy;
		this.name = name;
		this.lastName = lastName;
		this.birthday = birthday;
		this.gender = gender;
		this.job = job;
	}
	public EmployeeDto(Long idEmploy, String name) {
		super();
		this.idEmploy = idEmploy;
		this.name = name;
	

	}

	public Long getIdEmploy() {
		return idEmploy;
	}

	public void setIdEmploy(Long idEmploy) {
		this.idEmploy = idEmploy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Genders getGender() {
		return gender;
	}

	public void setGender(Genders gender) {
		this.gender = gender;
	}

	public Jobs getJob() {
		return job;
	}

	public void setJob(Jobs job) {
		this.job = job;
	}

	public EmployeeDto() {
	}

}
