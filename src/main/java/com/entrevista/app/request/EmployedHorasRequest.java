package com.entrevista.app.request;

public class EmployedHorasRequest {
	
	private String StarDate;
	private String endDate;
	private Long employeeId;

	
	public EmployedHorasRequest() {
		super();
	
	}
	
	public EmployedHorasRequest(String starDate, String endDate, Long employeeId) {
		super();
		StarDate = starDate;
		this.endDate = endDate;
		this.employeeId = employeeId;
	}
	
	public String getStarDate() {
		return StarDate;
	}
	public void setStarDate(String starDate) {
		StarDate = starDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	
}
