package com.entrevista.app.request;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class EmployeeRquest {
	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); 

	private String name;
	private String lastName;
	private String birthday;
	private Long genderId;
	private Long jobId;
	private Long idEmploy;

	public Long getIdEmploy() {
		return idEmploy;
	}

	public EmployeeRquest(Long idEmploy) {
		super();
		this.idEmploy = idEmploy;
	}

	public void setIdEmploy(Long idEmploy) {
		this.idEmploy = idEmploy;
	}

	public EmployeeRquest() {
		// TODO Auto-generated constructor stub
	}

	public EmployeeRquest(String name, String lastName, Date birthday, Long genderId, Long jobId) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.birthday = dateFormat.format(birthday);
		this.genderId = genderId;
		this.jobId = jobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
	

}
