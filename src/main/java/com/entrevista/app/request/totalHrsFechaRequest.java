package com.entrevista.app.request;

public class totalHrsFechaRequest {
    private Long employee_id;
    private String start_date;
    private String end_date;

    public totalHrsFechaRequest(Long employee_id) {
        this.employee_id = employee_id;
    }

    public totalHrsFechaRequest(Long employee_id, String start_date, String end_date) {
        this.employee_id = employee_id;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public Long getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Long employee_id) {
        this.employee_id = employee_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}


