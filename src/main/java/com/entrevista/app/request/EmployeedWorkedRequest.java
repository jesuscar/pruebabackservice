package com.entrevista.app.request;

import java.util.Date;



public class EmployeedWorkedRequest {
	private Long employeeId;
	private Integer workedHours;
	private String workedDate;
	
	public EmployeedWorkedRequest(Long employeeId, Integer workedHours, String workedDate) {
		super();
		this.employeeId = employeeId;
		this.workedHours = workedHours;
		this.workedDate = workedDate;
	}
	public EmployeedWorkedRequest(Long employeeId, Integer workedHours) {
		super();
		this.employeeId = employeeId;
		this.workedHours = workedHours;
		this.workedDate = workedDate;
	}
	public EmployeedWorkedRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getWorkedHours() {
		return workedHours;
	}

	public void setWorkedHours(Integer workedHours) {
		this.workedHours = workedHours;
	}

	public String getWorkedDate() {
		return workedDate;
	}

	public void setWorkedDate(String workedDate) {
		this.workedDate = workedDate;
	}

}
