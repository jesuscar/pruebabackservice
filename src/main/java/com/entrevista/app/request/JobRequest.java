package com.entrevista.app.request;

public class JobRequest {
    private Integer job_id;

    public JobRequest(Integer job_id) {
        this.job_id = job_id;
    }

    public Integer getJob_id() {
        return job_id;
    }

    public void setJob_id(Integer job_id) {
        this.job_id = job_id;
    }
}
