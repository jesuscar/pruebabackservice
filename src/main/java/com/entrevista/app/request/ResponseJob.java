package com.entrevista.app.request;

import com.entrevista.app.domain.Employees;
import com.entrevista.app.domain.Genders;
import com.entrevista.app.dto.EmployeeDto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ResponseJob {

    private ArrayList<EmployeeDto> empleados;

    public ResponseJob(ArrayList<EmployeeDto> empleados) {
        this.empleados = empleados;
    }

    public ArrayList<EmployeeDto> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList<EmployeeDto> empleados) {
        this.empleados = empleados;
    }
    /*
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    private String name;
    private String lastName;
    private String birthday;
    private Genders gender;
    private Long jobId;
    private Long idEmploy;

    public Long getIdEmploy() {
        return idEmploy;
    }

    public ResponseJob(Long idEmploy) {
        super();
        this.idEmploy = idEmploy;
    }

    public void setIdEmploy(Long idEmploy) {
        this.idEmploy = idEmploy;
    }

    public ResponseJob() {
        // TODO Auto-generated constructor stub
    }

    public ResponseJob(String name, String lastName, Date birthday, Genders gender, Long jobId) {
        super();
        this.name = name;
        this.lastName = lastName;
        this.birthday = dateFormat.format(birthday);
        this.jobId = jobId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Genders getGender() {
        return gender;
    }

    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }*/
}
