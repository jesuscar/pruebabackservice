package com.entrevista.app.controller;

import com.entrevista.app.request.*;
import com.entrevista.app.utilerias.GenericResponseArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import com.entrevista.app.dto.EmployeHoursDto;
import com.entrevista.app.dto.EmployeeDto;
import com.entrevista.app.service.EmployeesService;
import com.entrevista.app.utilerias.GenericResponsePrueba;

import java.lang.reflect.Array;
import java.util.ArrayList;

@RestController
@RequestMapping("empleados")
public class EmployeeController {

	@Autowired
	EmployeesService employeesService;
	@CrossOrigin
	@PostMapping(value = "/alta")
	public @ResponseBody ResponseEntity<?> altaEmpleado(@RequestBody EmployeeRquest employeeRquest) {
		try {

		 GenericResponsePrueba genericResponse = employeesService.altaEmpleado(employeeRquest);

			
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin
	@PostMapping(value = "/busquedaEmpleado")
	public @ResponseBody ResponseEntity<?> busquedaEmpleado(@RequestBody EmployeeRquest employeeRquest) {
		try {

		 EmployeeDto genericResponse = employeesService.obtenerempleadoPorId(employeeRquest);

			
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	@CrossOrigin
	@PostMapping(value = "/busquedaFechas")
	public @ResponseBody ResponseEntity<?> busquedaFecha(@RequestBody EmployedHorasRequest employedHorasRequest) {
		try {

		 EmployeHoursDto genericResponse = employeesService.obtenerempleadoPorHoras(employedHorasRequest);
			
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}


	@CrossOrigin
	@PostMapping(value = "/altaHrs")
	public @ResponseBody ResponseEntity<?> altaEmpleadoHrs(@RequestBody EmployeedWorkedRequest request) {
		try {

			GenericResponsePrueba genericResponse = employeesService.altaEmpleadoHrs(request);


			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin
	@PostMapping(value = "/obtenerPorIdJob")
	public @ResponseBody ResponseEntity<?> obtenerEmpleadosporPuesto(@RequestBody JobRequest request) {
		try {
			ResponseJob genericResponse = employeesService.obtenerEmpleadoJob(request);
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin
	@PostMapping(value = "/obtenertotalhrsFecha")
	public @ResponseBody ResponseEntity<?> obtenertotalhrsFecha(@RequestBody totalHrsFechaRequest request) {
		try {
			GenericResponsePrueba genericResponse = employeesService.obtenerTotalHrsFecha(request);
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);
		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	@CrossOrigin
	@PostMapping(value = "/obtenerpagoEmpleadofecha")
	public @ResponseBody ResponseEntity<?> obtenerpagoEmpleadofecha(@RequestBody totalHrsFechaRequest request) {
		try {
			GenericResponsePrueba genericResponse = employeesService.obtenerpagoEmpleadofecha(request);
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);
		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
