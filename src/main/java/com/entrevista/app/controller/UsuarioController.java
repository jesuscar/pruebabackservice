package com.entrevista.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import com.entrevista.app.dto.UsuarioDto;
import com.entrevista.app.request.UsuarioRequest;
import com.entrevista.app.service.UsuarioService;
import com.entrevista.app.utilerias.GenericResponse;

@RestController
@RequestMapping("usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	@CrossOrigin
	@GetMapping(value = "/todos")
	public @ResponseBody List<UsuarioDto> getUsuario() {
		return usuarioService.obtenerTodosLosUsuarios();
	}
	@CrossOrigin
	@PostMapping(value = "/alta")
	public @ResponseBody ResponseEntity<?> altaUsuario(@RequestBody UsuarioRequest usuarioRequest) {
		try {

		 GenericResponse genericResponse = usuarioService.altaUsuario(usuarioRequest);
			
			return new ResponseEntity<>(genericResponse, HttpStatus.OK);

		} catch (HttpServerErrorException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
