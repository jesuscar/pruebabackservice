package com.entrevista.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = -3746803914963457699L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USUARIOID", nullable = false)
	private Long usuarioId;
	
	@Column (name = "Nombre")
	private String nombre;
	
	@Column (name = "APELLIDO")
	private String apellido;
	
	@Column (name = "STATUS")
	private Integer status;
	

	public Usuario() {
		// TODO Auto-generated constructor stub
	}


	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}


	


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}
	

}
