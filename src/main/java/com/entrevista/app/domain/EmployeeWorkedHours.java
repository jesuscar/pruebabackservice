package com.entrevista.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "EMPLOYEE_WORKED_HOURS")
public class EmployeeWorkedHours implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_employe_worked", nullable = false)
	private Long idEmployWorked;

	@ManyToOne
	@JoinColumn(name="EMPLOYEE_ID")
	private Employees employeeId;

	
	@Column (name = "WORKED_HOURS")
	private Integer workedHours;
	
	@Column (name = "WORKED_DATE")
	private Date workedDate;

	
	public Employees getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Employees employeeId) {
		this.employeeId = employeeId;
	}

	public Long getIdEmployWorked() {
		return idEmployWorked;
	}

	public void setIdEmployWorked(Long idEmployWorked) {
		this.idEmployWorked = idEmployWorked;
	}

	public Integer getWorkedHours() {
		return workedHours;
	}

	public void setWorkedHours(Integer workedHours) {
		this.workedHours = workedHours;
	}

	public Date getWorkedDate() {
		return workedDate;
	}

	public void setWorkedDate(Date workedDate) {
		this.workedDate = workedDate;
	}
	
	
}
