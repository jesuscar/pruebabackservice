package com.entrevista.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "EMPLOYEES")
public class Employees implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_employ", nullable = false)
	private Long idEmploy;

	@Column (name = "NAME")
	private String name;
	
	@Column (name = "LAST_NAME")
	private String lastName;
	
	@Column (name = "BIRTHDAY")
	private Date birthday;
	
	@ManyToOne
	@JoinColumn(name="GENDER_ID")
	private Genders gender;
	
	@ManyToOne
	@JoinColumn(name="JOB_ID")
	private Jobs job;

	public Long getIdEmploy() {
		return idEmploy;
	}

	public void setIdEmploy(Long idEmploy) {
		this.idEmploy = idEmploy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Genders getGender() {
		return gender;
	}

	public void setGender(Genders gender) {
		this.gender = gender;
	}

	public Jobs getJob() {
		return job;
	}

	public void setJob(Jobs job) {
		this.job = job;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	
}
