package com.entrevista.app.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name= "EMPLOYEE_WORKED_HOURS")
public class EmployeeWorkedDateSum implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="EMPLOYEE_ID", nullable = false)
	private Long employeeId;

	@Column (name = "WORKED_DATE")
	private Date workedDate;

	@Column (name = "WORKED_HOURS")
	private Integer workedHours;

	public EmployeeWorkedDateSum(Long employeeId, Date workedDate, Integer workedHours) {
		this.employeeId = employeeId;
		this.workedDate = workedDate;
		this.workedHours = workedHours;
	}

	public EmployeeWorkedDateSum() {

	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public Date getWorkedDate() {
		return workedDate;
	}

	public Integer getWorkedHours() {
		return workedHours;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public void setWorkedDate(Date workedDate) {
		this.workedDate = workedDate;
	}

	public void setWorkedHours(Integer workedHours) {
		this.workedHours = workedHours;
	}
}
