package com.entrevista.app.serviceImp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import com.entrevista.app.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.entrevista.app.domain.EmployeeWorkedHours;
import com.entrevista.app.domain.Employees;
import com.entrevista.app.domain.Genders;
import com.entrevista.app.domain.Jobs;
import com.entrevista.app.dto.EmployeHoursDto;
import com.entrevista.app.dto.EmployeeDto;
import com.entrevista.app.repository.EmployedHoursRepository;
import com.entrevista.app.repository.EmployeeRepository;
import com.entrevista.app.repository.GenderRepository;
import com.entrevista.app.repository.JobRepository;
import com.entrevista.app.service.EmployeesService;
import com.entrevista.app.utilerias.GenericResponsePrueba;


@Service
public class EmployeeServiceImpl implements EmployeesService {
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
	@Autowired
	GenderRepository genderRepository;
	@Autowired
	JobRepository jobRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	EmployedHoursRepository employedHoursRepository;

	@Override
	public GenericResponsePrueba altaEmpleado(EmployeeRquest employeeRquest) {
		Integer conteo = employeeRepository.buscarPorNombreApellido(employeeRquest.getName(),employeeRquest.getLastName());
		Date date = new Date();
		Date fechaRequest= fechasConversion(employeeRquest.getBirthday());
		ZoneId timeZone = ZoneId.systemDefault();
		LocalDate getLocalDate = date.toInstant().atZone(timeZone).toLocalDate();
		LocalDate getLocalDaterequest = fechaRequest.toInstant().atZone(timeZone).toLocalDate();
		System.out.println(getLocalDate.getYear());
		System.out.println(getLocalDaterequest.getYear());
		Integer anio = getLocalDate.getYear();
		Integer aniorequest = getLocalDaterequest.getYear();
		Integer mayorEdad = anio-aniorequest;
		if (conteo>=1|| mayorEdad < 18){
			System.out.print("Sin registros por repeticion: ");
			return new GenericResponsePrueba(0,"false");
		} else {
			System.out.print("CONTEO: " + conteo);
			System.out.print("Fecha de cumple: ");
			System.out.print("Fecha de cumple: ");
			Genders genders = genderRepository.buscarPorId(employeeRquest.getGenderId());
			Jobs job = jobRepository.buscarPorId(employeeRquest.getJobId());
			//Employees empleado =employeeRepository.buscarPorId();
			Employees e = new Employees();
			//	Date inicialDate = fechasConversion((intoString(calendario.getString("start_date"), "/", 4)));
			Date cumple = fechasConversion(employeeRquest.getBirthday());

			System.out.print("Fecha de cumple: " + cumple);
			e.setName(employeeRquest.getName());
			e.setLastName(employeeRquest.getLastName());
			e.setGender(genders);
			e.setJob(job);
			e.setBirthday(cumple);
			employeeRepository.save(e);

			/*EmployeeWorkedHours eh = new EmployeeWorkedHours();
			eh.setWorkedDate(cumple);
			eh.setWorkedHours(2);
			//eh.setEmployeeId();
			employedHoursRepository.save(eh);*/
		}
		Integer Secuencia = employeeRepository.obtenerIdFinal();
		return new GenericResponsePrueba(Secuencia,"true");
	}

	
	private Date fechasConversion(String entrada) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

		String conversionAnio = entrada;
		//String fechaInicia = intoString(conversionAnio, "/", 7);

		@SuppressWarnings("deprecation")
		String fechaInicial = formatter.format(Date.parse(conversionAnio));
		Date convertido = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			convertido = dateFormat.parse(fechaInicial);
		} catch (Exception e) {
			System.out.println(e);

		}
		return convertido;
	}


	@Override
	public EmployeeDto obtenerempleadoPorId(EmployeeRquest employeeRquest) {

		if (employeeRquest.getJobId() == null) {
			System.out.print("Validar Registro");
		}	
		Employees emple = employeeRepository.buscarPorId(employeeRquest.getIdEmploy());
		Genders g = genderRepository.buscarPorId(emple.getGender().getIdGender());
		Jobs j =jobRepository.buscarPorId(emple.getJob().getIdJob());
		
		EmployeeDto e = new EmployeeDto();
		e.setIdEmploy(employeeRquest.getIdEmploy());
		e.setName(emple.getName());
		e.setLastName(emple.getLastName());
		e.setBirthday(emple.getBirthday());
		e.setGender(g);
		e.setJob(j);
		
		return e;
		
	}


	@Override
	public EmployeHoursDto obtenerempleadoPorHoras(EmployedHorasRequest employedHorasRequest) {
		
		Integer horas = employedHoursRepository.buscarPorfecha(employedHorasRequest.getEmployeeId() ,employedHorasRequest.getEndDate(), employedHorasRequest.getEndDate());
		EmployeHoursDto e = new EmployeHoursDto();
				
		e.setHoras(horas);
		
		return e ;
	}

	@Override
	public GenericResponsePrueba altaEmpleadoHrs(EmployeedWorkedRequest request) {
		try {
			Date date = new Date();
			Date fechaRequest= fechasConversion(request.getWorkedDate());
			ZoneId timeZone = ZoneId.systemDefault();
			LocalDate getLocalDateRequest = fechaRequest.toInstant().atZone(timeZone).toLocalDate();
			LocalDate getLocalDateNow = date.toInstant().atZone(timeZone).toLocalDate();

			System.out.println("Fecha de request fechaRequest :" +getLocalDateRequest );
			System.out.println("Fecha de ahora :" +getLocalDateNow );

			if (getLocalDateRequest.isBefore(getLocalDateNow)) {
				System.out.println("Fecha de request fechaRequest : is before a now");

				Employees empleado = employeeRepository.buscarPorId(request.getEmployeeId());
				System.out.println("Empleado : " + empleado.getName());
				//Integer conteoTablaHrs =employedHoursRepository.obtenerIdEmpleadohrs(empleado.getIdEmploy());

				Integer hrsTrabajadas = employedHoursRepository.obtenerSumaHrs(empleado.getIdEmploy())==null ? 0 :employedHoursRepository.obtenerSumaHrs(empleado.getIdEmploy());
				System.out.println("HRS trabajadas!!  " + hrsTrabajadas);
				SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
				Date fecha = formato.parse(request.getWorkedDate());
				if (empleado.getIdEmploy() != null && hrsTrabajadas < 20) {
					EmployeeWorkedHours eh = new EmployeeWorkedHours();
					eh.setEmployeeId(empleado);
					eh.setWorkedDate(fecha);
					eh.setWorkedHours(request.getWorkedHours());
					employedHoursRepository.save(eh);
				} else {
					return new GenericResponsePrueba(null, "false");
				}
			}else{
				return new GenericResponsePrueba(null, "false");
			}
		}catch (Exception e ){
			System.out.println("Error en  " + e);
		}


		Integer secuenciaHrs = employedHoursRepository.obtenerIdFinalHrs();
		System.out.println("Secuencia :  "+ secuenciaHrs);

		return new GenericResponsePrueba(secuenciaHrs,"true");
	}

	@Override
	public ResponseJob obtenerEmpleadoJob(JobRequest request) {
		ArrayList<EmployeeDto> employees = employeeRepository.buscarPorIdJob(Long.valueOf(request.getJob_id()));

		Genders genders ; new Genders();
		for(int i = 0 ; i < employees.size();i ++){
		//employees.get(i).setGender();
			genderRepository.buscarPorId(employees.get(i).getGender().getIdGender());

	}
		ResponseJob responseJob = new ResponseJob(employees);
		System.out.println("resultado aqui  "+ employees.get(0));
		return responseJob;
	}

	@Override
	public GenericResponsePrueba obtenerTotalHrsFecha(totalHrsFechaRequest request) {
		Integer hrsTrabajadas =0;
		try {

			Date fechaRequest= fechasConversion(request.getStart_date());
			Date fechaFinalRequest= fechasConversion(request.getEnd_date());
			ZoneId timeZone = ZoneId.systemDefault();
			LocalDate getLocalDateRequest = fechaRequest.toInstant().atZone(timeZone).toLocalDate();
			LocalDate getLocalDateNow = fechaFinalRequest.toInstant().atZone(timeZone).toLocalDate();

			System.out.println("Fecha de request fechaRequest :" +getLocalDateRequest );
			System.out.println("Fecha de ahora :" +getLocalDateNow );

			if (getLocalDateRequest.isBefore(getLocalDateNow)) {
				System.out.println("Fecha de request fechaRequest : is before a now");

				Employees empleado = employeeRepository.buscarPorId(request.getEmployee_id());
				System.out.println("Empleado : " + empleado.getName());
				//Integer conteoTablaHrs =employedHoursRepository.obtenerIdEmpleadohrs(empleado.getIdEmploy());
				SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
				Date fecha1 = formato.parse(request.getStart_date());
				Date fecha2 = formato.parse(request.getEnd_date());
				System.out.println("Fechas : " + fecha1 + fecha2);
				if (empleado.getIdEmploy() != null || fecha1.before(fecha2)) {
					 hrsTrabajadas = employedHoursRepository.obtenerSumaHrsfecha(request.getEmployee_id(), fecha1,fecha2) == null ? 0 : employedHoursRepository.obtenerSumaHrsfecha(request.getEmployee_id(), fecha1,fecha2);
					System.out.println("HRS trabajadas!!  " + hrsTrabajadas);
					//Date fecha = formato.parse(request.getWorkedDate());
				} else {
					return new GenericResponsePrueba(null, "false");
				}
			}
		}catch (Exception e ){
			System.out.println("Error en  " + e);
		}


		System.out.println("hrs :  "+ hrsTrabajadas);

		return new GenericResponsePrueba(hrsTrabajadas,"true");

	}

	@Override
	public GenericResponsePrueba obtenerpagoEmpleadofecha(totalHrsFechaRequest request) {
		float hrsTrabajadas =0;
		try {

			Date fechaRequest= fechasConversion(request.getStart_date());
			Date fechaFinalRequest= fechasConversion(request.getEnd_date());
			ZoneId timeZone = ZoneId.systemDefault();
			LocalDate getLocalDateRequest = fechaRequest.toInstant().atZone(timeZone).toLocalDate();
			LocalDate getLocalDateNow = fechaFinalRequest.toInstant().atZone(timeZone).toLocalDate();

			System.out.println("Fecha de request fechaRequest :" +getLocalDateRequest );
			System.out.println("Fecha de ahora :" +getLocalDateNow );

			if (getLocalDateRequest.isBefore(getLocalDateNow)) {
				System.out.println("Fecha de request fechaRequest : is before a now");

				Employees empleado = employeeRepository.buscarPorId(request.getEmployee_id());
				System.out.println("Empleado : " + empleado.getName());
				//Integer conteoTablaHrs =employedHoursRepository.obtenerIdEmpleadohrs(empleado.getIdEmploy());
				SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
				Date fecha1 = formato.parse(request.getStart_date());
				Date fecha2 = formato.parse(request.getEnd_date());
				System.out.println("Fechas : " + fecha1 + fecha2);
				if (empleado.getIdEmploy() != null || fecha1.before(fecha2)) {
					Jobs jobs =  jobRepository.buscarPorId(empleado.getJob().getIdJob());
					hrsTrabajadas = jobs.getSalary();
					
				} else {
					return new GenericResponsePrueba(null, "false");
				}
			}
		}catch (Exception e ){
			System.out.println("Error en  " + e);
		}


		System.out.println("hrs :  "+ hrsTrabajadas);
		int y = (int)hrsTrabajadas;
		return new GenericResponsePrueba(y,"true");

	}


}
