package com.entrevista.app.serviceImp;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.entrevista.app.domain.Usuario;
import com.entrevista.app.dto.UsuarioDto;
import com.entrevista.app.repository.UsuarioRepository;
import com.entrevista.app.request.UsuarioRequest;
import com.entrevista.app.service.UsuarioService;
import com.entrevista.app.utilerias.GenericResponse;

@Service
public class UsuarioServiceImp implements UsuarioService{
	@Autowired
	UsuarioRepository usuarioRepository;
	@Override
	public List<UsuarioDto> obtenerTodosLosUsuarios() {
		return usuarioRepository.traerTodosLosUsuarios();

	}
	@Override
	public GenericResponse altaUsuario(UsuarioRequest usuarioRequest) {
	if(usuarioRequest.getNombre()==null||usuarioRequest.getApellido()==null) {
throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Error en parametros");
		}
	if(usuarioRequest.getNombre()==""||usuarioRequest.getApellido()=="") {
throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Error en parametros");
		}
		Usuario u= new Usuario();
		u.setNombre(usuarioRequest.getNombre());
		u.setApellido(usuarioRequest.getApellido());
		u.setStatus(1);
		
		usuarioRepository.save(u);
		
		return new GenericResponse(200,"Se inserto Correctamente");
	}



}
