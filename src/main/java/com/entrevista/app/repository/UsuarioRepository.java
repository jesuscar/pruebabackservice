package com.entrevista.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.entrevista.app.domain.Usuario;
import com.entrevista.app.dto.UsuarioDto;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	@Query("SELECT new  com.entrevista.app.dto.UsuarioDto"
			+ " (u.usuarioId, u.nombre, u.apellido,u.status ) "
			+ " FROM com.entrevista.app.domain.Usuario u "
			+ " WHERE status = 1 ")

	List<UsuarioDto> traerTodosLosUsuarios();
}
