package com.entrevista.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.entrevista.app.domain.Jobs;

public interface JobRepository extends CrudRepository<Jobs, Long> {
	@Query("SELECT j" 
			+ " FROM com.entrevista.app.domain.Jobs j "
			+ "WHERE j.idJob =:idJob")
	Jobs buscarPorId(@Param("idJob") Long idJob);
}
