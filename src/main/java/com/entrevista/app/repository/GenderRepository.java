package com.entrevista.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.entrevista.app.domain.Genders;

public interface GenderRepository extends CrudRepository<Genders, Long>{
	@Query("SELECT g" 
			+ " FROM com.entrevista.app.domain.Genders g "
			+ "WHERE g.idGender =:idGender")
	Genders buscarPorId(@Param("idGender") Long idGender);

	

}
