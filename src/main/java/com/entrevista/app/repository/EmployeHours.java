package com.entrevista.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.entrevista.app.domain.EmployeeWorkedHours;

public interface EmployeHours extends CrudRepository<EmployeeWorkedHours, Long>{

    @Query("SELECT count(e) "
            + "FROM com.entrevista.app.domain.EmployeeWorkedHours e ")
    Integer obtenerIdFinalHrs();

}
