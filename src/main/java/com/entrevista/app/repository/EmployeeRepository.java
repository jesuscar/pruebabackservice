package com.entrevista.app.repository;

import com.entrevista.app.domain.Genders;
import com.entrevista.app.dto.EmployeeDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.entrevista.app.domain.Employees;

import java.util.ArrayList;

public interface EmployeeRepository extends CrudRepository<Employees, Long> {

	@Query("SELECT e" 
			+ " FROM com.entrevista.app.domain.Employees e "
			+ "WHERE e.idEmploy =:idEmploy")
	Employees buscarPorId(@Param("idEmploy") Long idEmploy);

	@Query("SELECT count(e) "
			+ "FROM com.entrevista.app.domain.Employees e "
			+ "WHERE e.name =:name AND e.lastName =:lastName")
	Integer buscarPorNombreApellido(@Param("name") String name,@Param("lastName") String lastName);

	@Query("SELECT count(e) "
			+ "FROM com.entrevista.app.domain.Employees e ")
	Integer obtenerIdFinal();

	@Query("SELECT e"
			+ " FROM com.entrevista.app.domain.Employees e "
			+ "WHERE e.job =:job")
	ArrayList<EmployeeDto> buscarPorIdJob(@Param("job") Long job);

}
