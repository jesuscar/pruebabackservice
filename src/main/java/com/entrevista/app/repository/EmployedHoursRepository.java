package com.entrevista.app.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.entrevista.app.domain.EmployeeWorkedHours;

public interface EmployedHoursRepository extends CrudRepository<EmployeeWorkedHours, Long> {
	
	//@Query("SELECT count(e) "
	@Query("SELECT  COUNT(e)" 
			+ " FROM com.entrevista.app.domain.EmployeeWorkedHours e "
			+ "WHERE e.workedDate BETWEEN :inicial AND :fina AND e.employeeId =:employeeId")
	Integer buscarPorfecha(@Param("employeeId") Long employeeId ,@Param("inicial") String inicial,@Param("fina") String fina);

	@Query("SELECT count(h) "
			+ " FROM com.entrevista.app.domain.EmployeeWorkedHours h ")
	Integer obtenerIdFinalHrs();

	@Query("SELECT Sum(e.workedHours) "
			+ "FROM com.entrevista.app.domain.EmployeeWorkedHoursSum e "
			+	"WHERE e.employeeId =:employeeId")
	Integer obtenerSumaHrs(@Param("employeeId")Long employeeId);
	@Query("SELECT count(h) "
			+ " FROM com.entrevista.app.domain.EmployeeWorkedHours h "
			+	"WHERE h.employeeId =:employeeId")
	Integer obtenerIdEmpleadohrs(@Param("employeeId")Long employeeId);
	@Query("SELECT Sum(e.workedHours) "
			+ "FROM com.entrevista.app.domain.EmployeeWorkedDateSum e "
			+	"WHERE e.employeeId =:employeeId AND e.workedDate BETWEEN :inicial AND :fina" )
			//"AND e.workedDate BETWEEN TO_DATE(:inicial ,'YYYY-MM-dd') AND" +
			//"  TO_DATE(:fina ,'YYYY-MM-dd') "
	//Integer obtenerSumaHrsfecha(@Param("employeeId")Long employeeId);

	Integer obtenerSumaHrsfecha(@Param("employeeId")Long employeeId,@Param("inicial")Date inicial,@Param("fina")Date fina);
}



